/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fish.payara;

import javax.annotation.Resource;
import javax.jms.*;
import javax.ejb.Stateless;

/**
 *
 * @author Luis Fernández
 */
@Stateless
public class MessageSender {

    @Resource(mappedName = "jms/JMSConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "jms/myQueue")
    Queue queue;

    public void sendMessage(String message) {
        MessageProducer messageProducer;//Send objects to destination
        TextMessage textMessage;
        try {
            Connection connection = connectionFactory.createConnection();//connectionFactory it´s uses for create a Connection object that musts be closed
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            messageProducer = session.createProducer(queue);
            textMessage = session.createTextMessage();

            textMessage.setText(message);
            messageProducer.send(textMessage);//sending message

            messageProducer.close();
            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
