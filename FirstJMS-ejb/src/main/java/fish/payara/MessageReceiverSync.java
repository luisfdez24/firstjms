/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fish.payara;

import java.util.Enumeration;
import java.util.LinkedList;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;
import java.util.LinkedList;


/**
 *
 * @author Luis Fernández
 */
@Stateless
public class MessageReceiverSync {

    @Resource(mappedName = "jms/JMSConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "jms/myQueue")
    Queue myQueue;

       
    
    public String receiveMessage() {
       String textAux = "";
       try {
                 
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            //MessageConsumer consumer=session.createConsumer(myQueue);
            //consumer.receive();
            QueueBrowser queueBrowser = session.createBrowser(myQueue);
            Enumeration enumeration = queueBrowser.getEnumeration();//returns the enumeration object that will be used to iterate through the queue
            while (enumeration.hasMoreElements()) {
                TextMessage o = (TextMessage) enumeration.nextElement();
                textAux=o.getText();
                //return "Recibido " + o.getText();
            }
            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();/*your main method will be called and inserted to stack, 
            then the second method will be called and inserted to the stack in LIFO order 
            and if any error occurs somewhere inside any method then this stack will help to identify that method.*/

        }
        return "Recibido " + textAux;
    }
}
