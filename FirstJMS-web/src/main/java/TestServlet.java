/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Luis Fernández
 */
//import com.walmeric.repository.DataSourceProvider;
//import javax.inject.Inject;
//import javax.jms.*;
import java.util.logging.Logger;
import fish.payara.MessageReceiverSync;
import fish.payara.MessageSender;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


@WebServlet(urlPatterns = {"/TestServlet"})
public class TestServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(TestServlet.class.getName());

//    @Inject
//    private DataSourceProvider dataSourceProvider;

    @EJB
    MessageSender sender;

    @EJB
    MessageReceiverSync receiver;
    
    private Connection connection;
    
    
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        String jsonStr = "{\"Customer\": {"
                + "\"address\": {"
                + "\"street\": \"NANTERRE CT\","
                + "\"postcode\": 77471"
                + "},"
                + "\"name\": \"Mary\","
                + "\"age\": 37"
                + "}}";//example of JSON to convert

        JSONObject json = new JSONObject(jsonStr);
        String xml = XML.toString(json);

        System.out.println(xml);
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            out.println(xml);
            out.println("");
            out.println("JMS Envia mensaje ");
            out.println("");
            out.println("");
            out.println("JMS envia/recibe mensaje usando JMS " + request.getContextPath() + "");
            String m = "Hola en test";
            sender.sendMessage(m);
            out.format("Mensaje enviado: %1$s.", m);
            out.println("Recibiendo mensaje...");
            String message = receiver.receiveMessage();
            out.println("Mensaje rx: " + message);
            out.println("");
            out.println("");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public void setOmnitureConversion(int clientId, int id, String date, String conversion_date, int uid, String header, String host, String data, int uploaded, int retries) {
        PreparedStatement preparedStatement = null;

        try {
//            String dbName = dataSourceProvider.getDbName(clientId);//creo que siempre es ringpool
//            connection = dataSourceProvider.getByClientId(clientId).getConnection();
            Connection conexion = DriverManager.getConnection ("jdbc:mysql://192.168.56.101/ringpool","root", "walmeric");
            preparedStatement = connection.prepareStatement("insert ringpool.omniture_conversion(id,date,conversion_date,uid,header,host,data,uploaded,retries) "
                    + "values(?,?,?,?,?,?,?,?,?)");
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, date);
            preparedStatement.setString(3, conversion_date);
            preparedStatement.setInt(4, uid);
            preparedStatement.setString(5, header);
            preparedStatement.setString(6, host);
            preparedStatement.setString(7, data);
            preparedStatement.setInt(8, uploaded);
            preparedStatement.setInt(9, retries);
            preparedStatement.executeUpdate();
            logger.log(Level.INFO, "ringpool insertando parametros en omniture_conversion "
                    , new Object[]{id, date, conversion_date,uid,header,host,data,uploaded,retries});
        } catch (SQLException e) {
            logger.log(Level.SEVERE, " ringpool insertando parametros en omniture_conversion" + e.getMessage(), e);
        } finally {
            close(preparedStatement);
            close(connection);
        }

    }
    
        public void setLogOmniture(int id, String date, String result, String response) {
        PreparedStatement preparedStatement = null;

        try {
            Connection conexion = DriverManager.getConnection ("jdbc:mysql://192.168.56.101/ringpool","root", "walmeric");
            preparedStatement = connection.prepareStatement("insert ringpool.log_omniture(id,date,result,respone) values(?,?,?,?)");
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, date);
            preparedStatement.setString(3, result);
            preparedStatement.setString(4, response);
            preparedStatement.executeUpdate();
            logger.log(Level.INFO, "ringpool insertando parametros en log_omniture"
                    , new Object[]{id, date, result, response});
        } catch (SQLException e) {
            logger.log(Level.SEVERE, " ringpool insertando parametros en log_omniture", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }

    }

    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
                statement = null;
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Error closing statement {} " + e.getMessage(), e);
            }
        }
    }

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Error closing connection {} " + e.getMessage(), e);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
    public String[] getElementsOfXml(File f) {//parse an xml and save in a String
        String s[] = new String[10];
        String[] tags = {"id", "date", "conversion_date", "uid", "header", "host", "data", "uploaded", "retries"};
        File file = new File("C:\\file.xml");
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();//quita redundancias que tenia al parsear
            for (int i = 0; i < tags.length; i++) {
                s[i] = document.getElementsByTagName(tags[i]).item(0).getTextContent();
            }

        } catch (IOException io) {
            logger.log(Level.WARNING, "IOExcepcion to be caught when elements were getting of an xml" + io.getMessage(), io);
        } catch (ParserConfigurationException ex) {
            logger.log(Level.WARNING, "ParserConfigurationException to be caught when elements were getting of an xml" + ex.getMessage(), ex);
        } catch (SAXException ex) {
            logger.log(Level.WARNING, "SAXException to be caught when elements were getting of an xml" + ex.getMessage(), ex);
        }
        return s;
    }            
}
